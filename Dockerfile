FROM python:3.9-buster

WORKDIR /app

ADD . /app/

RUN pip install -r requirements.txt

CMD [ "gunicorn", "--bind", "0.0.0.0:5000", "app:app" ]

