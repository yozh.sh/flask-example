from flask import Flask
import socket

app = Flask(__name__)

@app.route("/")
def index():
    hostname = socket.gethostname()
    local_ip = socket.gethostbyname(hostname)
    return f"<h2>!!! MY IP IS {local_ip} !!!</h2>"
